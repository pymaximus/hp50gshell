# standard modules
import unittest

# modules under test
import hp50gshell
from hp50gshell import hpdirlist


class TestHPDirList(unittest.TestCase):

    def test_parse_rdir_1(self):
        input = '''

        { HOME } 205642.
        TEST 57. Directory 59130.
        A2B 422. Directory 34392.
        APPS 2784.5 Directory 62791.
        PTpar 48.5 Library Data 16554.
        TRIANGLES 28864.5 Directory 55733.
        EQUATION 2024. Directory 4503.
        IOPAR 29.5 List 45664.
        UTILS 3989. Directory 18236.
        BACKUP 100. Directory 28007.
        MYEQNS 361.5 Directory 4275.
        CASDIR 527. Directory 20560.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME', hpdl.dir_path)
        self.assertEqual(['HOME'], hpdl.dir_path_list)
        self.assertEqual('205642.', hpdl.dir_size)
        self.assertEqual(['TEST', '57.', 'Directory', '59130.'], hpdl.dirents[0])
        self.assertEqual(11, len(hpdl.dirents))

    def test_parse_rdir_2(self):
        input = '''

        { HOME APPS DPLANK } 205742.
        TEST 57. Directory 59130.
        A2B 422. Directory 34392.
        PTpar 48.5 Library Data 16554.
        TRIANGLES 28864.5 Directory 55733.
        EQUATION 2024. Directory 4503.
        IOPAR 29.5 List 45664.
        UTILS 3989. Directory 18236.
        MYEQNS 361.5 Directory 4275.
        CASDIR 527. Directory 20560.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME APPS DPLANK', hpdl.dir_path)
        self.assertEqual(['HOME', 'APPS', 'DPLANK'], hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(['PTpar', '48.5', 'Library Data', '16554.'],
                         hpdl.dirents[2])
        self.assertEqual(9, len(hpdl.dirents))



    def test_parse_rdir_3(self):
        """Directory has { or} in name, but it is not the top dir path"""
        input = '''

        { HOME APPS DPLANK } 205742.
        TEST 57. Directory 59130.
        A2B 422. Directory 34392.
        PTpar 48.5 Library Data 16554.
        TRIANGLES 28864.5 Directory 55733.
        EQUATION 2024. Directory 4503.
        IOP{AR 29.5 List 45664.
        UTILS 3989. Directory 18236.
        MYE}QNS 361.5 Directory 4275.
        CASDIR 527. Directory 20560.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME APPS DPLANK', hpdl.dir_path)
        self.assertEqual(['HOME', 'APPS', 'DPLANK'], hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(['PTpar', '48.5', 'Library Data', '16554.'],
                         hpdl.dirents[2])
        self.assertEqual(['IOP{AR', '29.5', 'List', '45664.'], hpdl.dirents[5])
        self.assertEqual(9, len(hpdl.dirents))


    def test_parse_rdir_4(self):
        """Directory entries have spaces in the names"""
        input = '''

        { HOME EQUATION TRIGONOMETRY } 205651.
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebraic 14762.
        SINE RULE A-B 53. Algebraic 52659.
        B 16. Real Number 11968.
        COS RULE C 81.5 Algebraic 23816.
        COS RULE B 81.5 Algebraic 12348.
        b 16. Real Number 51968.
        A 16. Real Number 37254.
        c 16. Real Number 43428.
        a 16. Real Number 51784.
        COS RULE A 81.5 Algebraic 7845.

        '''

        hpdl = hpdirlist.HPDirList()
        actual = hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME EQUATION TRIGONOMETRY', hpdl.dir_path)
        self.assertEqual(['HOME', 'EQUATION', 'TRIGONOMETRY'],
                         hpdl.dir_path_list)
        self.assertEqual('205651.', hpdl.dir_size)
        self.assertEqual(['TRIANGLE AREA', '60.5', 'Algebraic', '61717.'],
                         hpdl.dirents[0])
        self.assertEqual(['HERONS RULE', '26.', 'Real Number', '13207.'],
                         hpdl.dirents[1])
        self.assertEqual(14, len(hpdl.dirents))

    def test_parse_rdir_5(self):
        """TEST: Empty Directory"""
        input = '''

        { HOME APPS FOOBAR } 205742.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)
        self.assertEqual('HOME APPS FOOBAR', hpdl.dir_path)
        self.assertEqual(['HOME', 'APPS', 'FOOBAR'], hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(0, len(hpdl.dirents))

    def test_parse_rdir_6(self):
        """TEST: Empty Directory, that spans over 1 line"""
        input = '{ HOME APPS FOOBAR CAT \nDOG \n} 205742.'
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual(['HOME', 'APPS', 'FOOBAR', 'CAT', 'DOG'],
                         hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(0, len(hpdl.dirents))

    def test_parse_rdir_7(self):
        """TEST: Empty Directory, that spans over 2 line"""
        input = '{ HOME APPS FOOBAR CAT DOG \nAPPLE} 205742.'

        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)
        self.assertEqual('HOME APPS FOOBAR CAT DOG  APPLE', hpdl.dir_path)
        self.assertEqual(['HOME', 'APPS', 'FOOBAR', 'CAT', 'DOG', 'APPLE'],
                         hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(0, len(hpdl.dirents))

    def test_parse_rdir_8(self):
        """Input contains linefeeds"""
        input = '''

        { HOME
 APPS DPLANK } 205742.
        TEST 57. Directory 59130.
        A2B 422. Directory 34392.
        PTpar 48.5 Library Data 16554.
        TRIANGLES 28864.5 Directory 55733.
        EQUATION 
2024. Directory 4503.
        IOPAR 29.5 List 45664.
        UTILS 3989. Directory 18236.
        MYEQNS 361.5 Directory 4275.
        CASDIR 527. Directory 20560.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        #self.assertEqual('HOME APPS DPLANK', hpdl.dir_path)
        self.assertEqual(['HOME','APPS','DPLANK'], hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(['PTpar', '48.5', 'Library Data', '16554.'], hpdl.dirents[2])
        self.assertEqual(9, len(hpdl.dirents))

    def test_parse_rdir_9(self):
        """Input contains linefeeds, dir lengthj has digit after decimal point"""
        input = '''

        { HOME APPS
DPLANK } 205742.
        TEST 57. Directory 59130.
        A2B 422. Directory 34392.
        PTpar 48.5 Library Data 16554.
        TRIANGLES 28864.5 Directory 55733.
        EQUATION 2024. Directory 4503.
        IOPAR 29.5 List 45664.
        UTILS 3989. Directory 18236.
        MYEQNS 361.5 Directory 4275.
        CASDIR 527. Directory 20560.

        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME APPS DPLANK', hpdl.dir_path)
        self.assertEqual(['HOME','APPS','DPLANK'], hpdl.dir_path_list)
        self.assertEqual('205742.', hpdl.dir_size)
        self.assertEqual(['PTpar', '48.5', 'Library Data', '16554.'], hpdl.dirents[2])
        self.assertEqual(9, len(hpdl.dirents))

    def test_parse_rdir_10(self):
        """"""
        input = '''
[{ HOME EQUATION ELECTRICAL SPEED
ABC } 171678.5]
        '''
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME EQUATION ELECTRICAL SPEED ABC', hpdl.dir_path)
        self.assertEqual(['HOME','EQUATION','ELECTRICAL','SPEED','ABC'], hpdl.dir_path_list)
        self.assertEqual('171678.5', hpdl.dir_size)
        self.assertEqual(0, len(hpdl.dirents))



    def test_parse_rdir_11_incomplete_rdir(self):

        input = '''

        { HOME EQUATION TRIGONOMETRY } 205651.
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebraic 
        '''

        

        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(input)
        print(input)
        print(hpdl)

        self.assertEqual('HOME EQUATION ELECTRICAL SPEED ABC', hpdl.dir_path)
        self.assertEqual(['HOME','EQUATION','ELECTRICAL','SPEED','ABC'], hpdl.dir_path_list)
        self.assertEqual('171678.5', hpdl.dir_size)
        self.assertEqual(0, len(hpdl.dirents))

        
if __name__ == '__main__':
    unittest.main()
