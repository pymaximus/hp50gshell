# standard modules
import unittest

# modules under test
import hp50g

expected = '''{ HOME } 205752.
PPAR 83. List 40924.
T 13.5 String 46728.
Tr.3 15. Directory 24171.
TEST 83.5 Directory 34789.
A2B 422. Directory 34392.
APPS 4031.5 Directory 30866.
PTpar 48.5 Library Data 16554.
TRIANGLES 28864.5 Directory 55733.
EQUATION 2057. Directory 5863.
IOPAR 29.5 List 45664.
UTILS 3989. Directory 18236.
BACKUP 100. Directory 28007.
MYEQNS 361.5 Directory 4275.
CASDIR 527. Directory 20560.
'''


class TestStress(unittest.TestCase):

    def test_hp_cmd(self):
        hp = hp50g.HP50G()

        cmd = 'rhost "3 TRANSIO"'
        resp_stdout = hp.hp_cmd(cmd)

        cmd = 'rhost HOME'
        resp_stdout = hp.hp_cmd(cmd)

        while(True):
            cmd = 'rdir'
            resp_stdout = hp.hp_cmd(cmd)

            # self.assertTrue('APPS' in resp_stdout)
            self.assertTrue('CASDIR' in resp_stdout)
            # self.assertEqual(expected, resp_stdout)


if __name__ == '__main__':
    unittest.main()
