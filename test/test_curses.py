import curses
from curses import wrapper

import time

def main2(stdscr):

    # Make stdscr.getch non-blocking
    stdscr.nodelay(True)
    stdscr.clear()
    width = 4
    count = 0
    direction = 1
    while True:
        c = stdscr.getch()
        # Clear out anything else the user has typed in
        curses.flushinp()
        stdscr.clear()
        # If the user presses p, increase the width of the springy bar
        if c == ord('p'):
            width += 1
        # Draw a springy bar
        stdscr.addstr("#" * count)
        count += direction
        if count == width:
            direction = -1
        elif count == 0:
            direction = 1
        # Wait 1/10 of a second. Read below to learn about how to avoid
        # problems with using time.sleep with getch!
        time.sleep(0.1)


def main(stdscr):
    height,width = stdscr.getmaxyx()    
    mypad = curses.newpad(height, width)
    mypad.scrollok

    
    dir = ['HOME','APPS','Foo','Bar','CASDIR','TEMP']

    for f in dir:
        mypad.addstr(f)
    # # Make stdscr.getch non-blocking
    stdscr.nodelay(True)
    stdscr.clear()

    mypad_pos = 0
    mypad.border(1)
    mypad.refresh(mypad_pos, 0, 5, 5, 10, 60)
    #time.sleep(5)
    # width = 4
    # count = 0
    # direction = 1
    while True:
        cmd = stdscr.getch()
        # Clear out anything else the user has typed in
        curses.flushinp()
        stdscr.clear()

        if  cmd == curses.KEY_DOWN:
            mypad_pos += 1
            mypad.refresh(mypad_pos, 0, 5, 5, 10, 60)
        elif cmd == curses.KEY_UP:
            mypad_pos -= 1
        elif cmd == ord('q'):
            break  # Exit the while loop

        mypad.border(2)        
        mypad.refresh(mypad_pos, 0, 5, 5, 10, 60)        
    #     # If the user presses p, increase the width of the springy bar
    #     if c == ord('p'):
    #         width += 1
    #     # Draw a springy bar
    #     stdscr.addstr("#" * count)
    #     count += direction
    #     if count == width:
    #         direction = -1
    #     elif count == 0:
    #         direction = 1
    #     # Wait 1/10 of a second. Read below to learn about how to avoid
    #     # problems with using time.sleep with getch!
        time.sleep(0.1)
        
if __name__ == '__main__':
    wrapper(main)

    
