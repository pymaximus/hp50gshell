# standard modules
import unittest

# modules under test
from hp50gshell import hputils


class Testutils(unittest.TestCase):

    # Top Dir tests

    def test_dir_1(self):
        input = r'   { HOME EQUATION TRIGONOMETRY } 205651.  '
        expected = {'path': 'HOME EQUATION TRIGONOMETRY', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_2(self):
        input = r'   { HOME EQUATION } 205651.  '
        expected = {'path': 'HOME EQUATION', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_3(self):
        input = r'   { HOME   } 205651.  '
        expected = {'path': 'HOME', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_4(self):
        """Directory has single linefeed character"""
        input = '   { HOME FOO BAR CAT \nAPPLE BANANA  } 205651.  '
        print(input)
        expected = {'path': 'HOME FOO BAR CAT APPLE BANANA', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_5(self):
        # FIXME: Is this a response I can get from the calculator
        """Directory has multiple linefeed characters"""
        input = '   { HOME FOO BAR CAT \nAPPLE BAN\nANA  } 205651.  '
        print(input)
        expected = {'path': 'HOME FOO BAR CAT APPLE BANANA', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_6(self):
        input = '''

        { HOME EQUATION } 205651.
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebraic 14762.
        SINE RULE A-B 53. Algebraic 52659.
        B 16. Real Number 11968.
        COS RULE C 81.5 Algebraic 23816.
        COS RULE B 81.5 Algebraic 12348.
        b 16. Real Number 51968.
        A 16. Real Number 37254.
        c 16. Real Number 43428.
        a 16. Real Number 51784.
        COS RULE A 81.5 Algebraic 7845.

        '''
        expected = {'path': 'HOME EQUATION', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_7(self):
        input = '''

        { HOME
EQUATION } 205651.
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebr
aic 14762.
        SINE RULE A-B 53. Algebraic 52659.
        B 16. Real Number 11968.
        COS RULE C 81.5 Algebraic 23816.
        COS RU
LE B 81.5 Algebraic 12348.
        b 16. Real Number 51968.
        A 16. Real Number 37254.
        c 16. Real Number 43428.
        a 16. Real Number 51784.
        COS RULE A 81.5 Algebraic 7845.

        '''
        expected = {'path': 'HOME EQUATION', 'size': '205651.'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_dir_7b(self):
        input = '''
     { HOME EQUATION ELECTRICAL SPEED
ABC } 171678.5
        '''
        expected = {'path': 'HOME EQUATION ELECTRICAL SPEED ABC', 'size': '171678.5'}
        actual = hputils.get_dir_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    #
    # Dir entry tests
    #

    def test_dirent_1(self):
        """leading space"""
        input = r' TRIANGLES 28864.5 Directory 55733.'
        expected = {'name': 'TRIANGLES', 'size': '28864.5', 'checksum': '55733.', 'type': 'Directory'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_1b(self):
        """no leading space"""
        input = r'TRIANGLES 28864.5 Directory 55733.'
        expected = {'name': 'TRIANGLES', 'size': '28864.5', 'checksum': '55733.', 'type': 'Directory'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_2(self):
        input = r' BIG TRIANGLES 28864.5 Directory 55733.'
        expected = {'name': 'BIG TRIANGLES', 'size': '28864.5', 'checksum': '55733.', 'type': 'Directory'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_3(self):
        input = r' TRIANGLES 28864.5 Library Data 55733.'
        expected = {'name': 'TRIANGLES', 'size': '28864.5', 'checksum': '55733.', 'type': 'Library Data'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_4(self):
        input = r'COS RULE C 81.5 Algebraic 23816.'
        expected = {'name': 'COS RULE C', 'size': '81.5', 'type': 'Algebraic', 'checksum': '23816.'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_5(self):
        input = r'  SINE RULE B-C 53. Algebraic 5137.'
        expected = {'name': 'SINE RULE B-C', 'size': '53.', 'checksum': '5137.', 'type': 'Algebraic'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(1, len(actual))
        self.assertEqual(expected, actual[0])

    def test_dirent_6(self):
        """Dirent in larger rdir output"""

        input = '''

        { HOME EQUATION } 205651.
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.5
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebraic 14762.
        SINE RULE A-B 53. Algebraic 52659.
        B 16. Real Number 11968.
        COS RULE C 81.5 Algebraic 23816.
        COS RULE B 81.5 Algebraic 12348.
        b 16. Real Number 51968.
        A 16. Real Number 37254.
        c 16. Real Number 43428.
        a 16. Real Number 51784.
        COS RULE A 81.5 Algebraic 7845.

        '''

        expected = [
            {'name': 'TRIANGLE AREA', 'size': '60.5', 'type': 'Algebraic', 'checksum': '61717.'},
            {'name': 'HERONS RULE', 'size': '26.', 'type': 'Real Number', 'checksum': '13207.'},
            {'name': 'C', 'size': '16.', 'type': 'Real Number', 'checksum': '10767.5'},
            {'name': 'SINE RULE B-C', 'size': '53.', 'type': 'Algebraic', 'checksum': '5137.'},
            {'name': 'SINE RULE A-C', 'size': '53.', 'type': 'Algebraic', 'checksum': '14762.'},
            {'name': 'SINE RULE A-B', 'size': '53.', 'type': 'Algebraic', 'checksum': '52659.'},
            {'name': 'B', 'size': '16.', 'type': 'Real Number', 'checksum': '11968.'},
            {'name': 'COS RULE C', 'size': '81.5', 'type': 'Algebraic', 'checksum': '23816.'},
            {'name': 'COS RULE B', 'size': '81.5', 'type': 'Algebraic', 'checksum': '12348.'},
            {'name': 'b', 'size': '16.', 'type': 'Real Number', 'checksum': '51968.'},
            {'name': 'A', 'size': '16.', 'type': 'Real Number', 'checksum': '37254.'},
            {'name': 'c', 'size': '16.', 'type': 'Real Number', 'checksum': '43428.'},
            {'name': 'a', 'size': '16.', 'type': 'Real Number', 'checksum': '51784.'},
            {'name': 'COS RULE A', 'size': '81.5', 'type': 'Algebraic', 'checksum': '7845.'}
        ]
        actual = hputils.get_dirent_details(input)
        self.assertEqual(expected, actual)
        self.assertEqual(14, len(actual))

    def test_dirent_7(self):
        """Dirent in larger rdir output, dir size has number after decimal"""

        input = '''

        { HOME EQUATION } 205651.5
        TRIANGLE AREA 60.5 Algebraic 61717.
        HERONS RULE 26. Real Number 13207.
        C 16. Real Number 10767.5
        SINE RULE B-C 53. Algebraic 5137.
        SINE RULE A-C 53. Algebraic 14762.
        SINE RULE A-B 53. Algebraic 52659.
        B 16. Real Number 11968.
        COS RULE C 81.5 Algebraic 23816.
        COS RULE B 81.5 Algebraic 12348.
        b 16. Real Number 51968.
        A 16. Real Number 37254.
        c 16. Real Number 43428.
        a 16. Real Number 51784.
        COS RULE A 81.5 Algebraic 7845.

        '''

        expected = [
            {'name': 'TRIANGLE AREA', 'size': '60.5', 'type': 'Algebraic', 'checksum': '61717.'},
            {'name': 'HERONS RULE', 'size': '26.', 'type': 'Real Number', 'checksum': '13207.'},
            {'name': 'C', 'size': '16.', 'type': 'Real Number', 'checksum': '10767.5'},
            {'name': 'SINE RULE B-C', 'size': '53.', 'type': 'Algebraic', 'checksum': '5137.'},
            {'name': 'SINE RULE A-C', 'size': '53.', 'type': 'Algebraic', 'checksum': '14762.'},
            {'name': 'SINE RULE A-B', 'size': '53.', 'type': 'Algebraic', 'checksum': '52659.'},
            {'name': 'B', 'size': '16.', 'type': 'Real Number', 'checksum': '11968.'},
            {'name': 'COS RULE C', 'size': '81.5', 'type': 'Algebraic', 'checksum': '23816.'},
            {'name': 'COS RULE B', 'size': '81.5', 'type': 'Algebraic', 'checksum': '12348.'},
            {'name': 'b', 'size': '16.', 'type': 'Real Number', 'checksum': '51968.'},
            {'name': 'A', 'size': '16.', 'type': 'Real Number', 'checksum': '37254.'},
            {'name': 'c', 'size': '16.', 'type': 'Real Number', 'checksum': '43428.'},
            {'name': 'a', 'size': '16.', 'type': 'Real Number', 'checksum': '51784.'},
            {'name': 'COS RULE A', 'size': '81.5', 'type': 'Algebraic', 'checksum': '7845.'}
        ]
        actual = hputils.get_dirent_details(input)
        self.assertEqual(expected, actual)
        self.assertEqual(14, len(actual))

    def test_dirent_8(self):
        r"""A𝝿T (A\GmT) in var name"""

        input = r'''

{ HOME } 172794.5
A\GmT 14. Directory 24171.
Y1 50. Program 46322.
FIRST 37. Program 37456.
ACCU.HP 33538.5 Program 18491.
TEST 57. Directory 59130.
A2B 441. Directory 6541.
APPS 2867.5 Directory 57988.
PTpar 48.5 Library Data 16554.
TRIANGLES 28864.5 Directory 55733.
EQUATION 2540. Directory 64227.
IOPAR 29.5 List 45664.
UTILS 3989. Directory 18236.
BACKUP 100. Directory 28007.
MYEQNS 361.5 Directory 4275.
CASDIR 527. Directory 20560.

        '''

        expected = [
            {'name': 'A\\GmT', 'size': '14.', 'type': 'Directory', 'checksum': '24171.'},
            {'name': 'Y1', 'size': '50.', 'type': 'Program', 'checksum': '46322.'},
            {'name': 'FIRST', 'size': '37.', 'type': 'Program', 'checksum': '37456.'},
            {'name': 'ACCU.HP', 'size': '33538.5', 'type': 'Program', 'checksum': '18491.'},
            {'name': 'TEST', 'size': '57.', 'type': 'Directory', 'checksum': '59130.'},
            {'name': 'A2B', 'size': '441.', 'type': 'Directory', 'checksum': '6541.'},
            {'name': 'APPS', 'size': '2867.5', 'type': 'Directory', 'checksum': '57988.'},
            {'name': 'PTpar', 'size': '48.5', 'type': 'Library Data', 'checksum': '16554.'},
            {'name': 'TRIANGLES', 'size': '28864.5', 'type': 'Directory', 'checksum': '55733.'},
            {'name': 'EQUATION', 'size': '2540.', 'type': 'Directory', 'checksum': '64227.'},
            {'name': 'IOPAR', 'size': '29.5', 'type': 'List', 'checksum': '45664.'},
            {'name': 'UTILS', 'size': '3989.', 'type': 'Directory', 'checksum': '18236.'},
            {'name': 'BACKUP', 'size': '100.', 'type': 'Directory', 'checksum': '28007.'},
            {'name': 'MYEQNS', 'size': '361.5', 'type': 'Directory', 'checksum': '4275.'},
            {'name': 'CASDIR', 'size': '527.', 'type': 'Directory', 'checksum': '20560.'}
            ]

        actual = hputils.get_dirent_details(input)
        print(actual)

        self.assertEqual(15, len(actual))
        self.assertEqual(expected, actual)

    def Xtest_dirent_66(self):
        """EEntry has \n in text"""
        input = '  SINE RU\nLE B-C 53. Algebraic 5137.'
        expected = {'name': 'SINE RULE B-C', 'size': '53.', 'checksum': '5137.', 'type': 'Algebraic'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def Xtest_dirent_67(self):
        """Entry has \n in text"""
        input = '  SINE RU\nLE B-C 53. Algebraic 5137.'
        expected = {'name': 'SINE RULE B-C', 'size': '53.', 'checksum': '5137.', 'type': 'Algebraic'}
        actual = hputils.get_dirent_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    #
    # Test PATH functions
    #
    def test_path_details_1(self):
        input = '   { HOME EQUATION TRIGONOMETRY }  '
        expected = {'path': 'HOME EQUATION TRIGONOMETRY'}
        actual = hputils.get_path_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    def test_path_details_2(self):
        input = '   { HOME EQUATION\n TRIGONOMETRY }  '
        expected = {'path': 'HOME EQUATION TRIGONOMETRY'}
        actual = hputils.get_path_details(input)
        print(actual)
        self.assertEqual(expected, actual)

    #
    # PC <--> CALCULATOR directory path name conversions
    #

    def test_pc_dir_from_calc_dir_1(self):
        actual = hputils.pc_dir_from_calc_dir(['HOME'])
        expected = '/'
        self.assertEqual(expected, actual)

    def test_pc_dir_from_calc_dir_2(self):
        actual = hputils.pc_dir_from_calc_dir(['HOME', 'APPS'])
        expected = '/APPS'
        self.assertEqual(expected, actual)

    def test_pc_dir_from_calc_dir_3(self):
        actual = hputils.pc_dir_from_calc_dir(['HOME', 'APPS', 'PLANK'])
        expected = '/APPS/PLANK'
        self.assertEqual(expected, actual)

    def test_calc_dir_from_pc_dir_1(self):
        actual = hputils.calc_dir_from_pc_dir('/')
        expected = ['HOME']
        self.assertEqual(expected, actual)

    def test_calc_dir_from_pc_dir_2(self):
        actual = hputils.calc_dir_from_pc_dir('/APPS')
        expected = ['HOME', 'APPS']
        self.assertEqual(expected, actual)

    def test_calc_dir_from_pc_dir_3(self):
        actual = hputils.calc_dir_from_pc_dir('/APPS/PLANK')
        expected = ['HOME', 'APPS', 'PLANK']
        self.assertEqual(expected, actual)

    #
    # TVIEW tests
    #

    def test_tview_parse_1(self):
        """Test regex that parses TVIEW"""

        input = r'''

%%HP: T(3)A(R)F(.);
{ :/HOME/HPTREE: 8. :/HOME/RVIS:
8. :/HOME/UNIXPATH: 8.
:/HOME/HPTREE2.HPPRG: 8.
:/HOME/VISITOR.TXT: 8.
:/HOME/CNTDWN~1.HPP: 2.
:/HOME/A\piT: 15.
:/HOME/A\piT/FRED\pi\Gt\Gl: 15.
:/HOME/A\piT/A: 0. :/HOME/Y1: 8.
:/HOME/FIRST: 8. :/HOME/ACCU.HP:
8. :/HOME/TEST: 15.
:/HOME/TEST/ACIR: 8. :/HOME/A2B:
15. :/HOME/A2B/CNTDWN: 8.
:/HOME/A2B/UP: 8.
:/HOME/A2B/ASCIIBIN.49: 8.
:/HOME/CASDIR/MODULO: 28.
:/HOME/CASDIR/PRIMIT: 9.
:/HOME/CASDIR/CASINFO: 11.
:/HOME/CASDIR/REALASSUME: 5.
:/HOME/CASDIR/VX: 6. }

'''

        expected = [{'name': '/HOME/HPTREE', 'type': '8.'},
                    {'name': '/HOME/RVIS', 'type': '8.'},
                    {'name': '/HOME/UNIXPATH', 'type': '8.'},
                    {'name': '/HOME/HPTREE2.HPPRG', 'type': '8.'},
                    {'name': '/HOME/VISITOR.TXT', 'type': '8.'},
                    {'name': '/HOME/CNTDWN~1.HPP', 'type': '2.'},
                    {'name': '/HOME/A\\piT', 'type': '15.'},
                    {'name': '/HOME/A\\piT/FRED\\pi\\Gt\\Gl', 'type': '15.'},
                    {'name': '/HOME/A\\piT/A', 'type': '0.'},
                    {'name': '/HOME/Y1', 'type': '8.'},
                    {'name': '/HOME/FIRST', 'type': '8.'},
                    {'name': '/HOME/ACCU.HP', 'type': '8.'},
                    {'name': '/HOME/TEST', 'type': '15.'},
                    {'name': '/HOME/TEST/ACIR', 'type': '8.'},
                    {'name': '/HOME/A2B', 'type': '15.'},
                    {'name': '/HOME/A2B/CNTDWN', 'type': '8.'},
                    {'name': '/HOME/A2B/UP', 'type': '8.'},
                    {'name': '/HOME/A2B/ASCIIBIN.49', 'type': '8.'},
                    {'name': '/HOME/CASDIR/MODULO', 'type': '28.'},
                    {'name': '/HOME/CASDIR/PRIMIT', 'type': '9.'},
                    {'name': '/HOME/CASDIR/CASINFO', 'type': '11.'},
                    {'name': '/HOME/CASDIR/REALASSUME', 'type': '5.'},
                    {'name': '/HOME/CASDIR/VX', 'type': '6.'}]

        actual = hputils.get_tview_details(input)

        print(actual)
        self.assertEqual(23, len(actual))
        self.assertEqual(expected, actual)
        self.assertEqual({'name': '/HOME/HPTREE', 'type': '8.'}, actual[0])
        self.assertEqual({'name': '/HOME/RVIS', 'type': '8.'}, actual[1])
        self.assertEqual({'name': '/HOME/HPTREE', 'type': '8.'}, actual[0])


if __name__ == '__main__':
    unittest.main(verbosity=3)
