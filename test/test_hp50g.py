# standard modules
import unittest

# modules under test

from hp50gshell import hp50g


class TestAdd(unittest.TestCase):
    """
    Test the add function from the mymath library
    """

    def test_hp_cmd(self):
        hpc = hp50g.HP50G()
        cmd = 'rhost "6 FIX DATE"'
        resp_stdout = hpc.hp_cmd(cmd)

        self.assertTrue('2019' in resp_stdout)

    def test_hp_cmd_math_add(self):
        hpc = hp50g.HP50G()
        hpc.hp_cmd('rhost CLEAR')
        cmd = 'rhost "55 3 +"'
        resp_stdout = hpc.hp_cmd(cmd)

        self.assertTrue('58' in resp_stdout)

    def test_hp_cmd_math_subtract(self):
        hpc = hp50g.HP50G()
        hpc.hp_cmd('rhost CLEAR')
        cmd = 'rhost "55 3 -"'
        resp_stdout = hpc.hp_cmd(cmd)

        self.assertTrue('52' in resp_stdout)

    def test_hp_cmd_math_multiply(self):
        hpc = hp50g.HP50G()
        hpc.hp_cmd('rhost CLEAR')
        cmd = 'rhost "4 6 *"'
        resp_stdout = hpc.hp_cmd(cmd)

        self.assertTrue('24' in resp_stdout)

    def test_hp_cmd_math_divide(self):
        hpc = hp50g.HP50G()
        hpc.hp_cmd('rhost CLEAR')
        cmd = 'rhost "21 3 /"'
        resp_stdout = hpc.hp_cmd(cmd)

        self.assertTrue('7' in resp_stdout)

    def test_hp_parse_stack_string(self):
        hpc = hp50g.HP50G()
        stack_string = '''
        3:        42.
        2:        23.072019
        1:        24.072019
        '''

        stack = hpc.hp_parse_stack_string(stack_string)
        self.assertEqual('42.', stack[3-1])
        self.assertEqual('23.072019', stack[2-1])
        self.assertEqual('24.072019', stack[1-1])

    def test_hp_parse_stack_string_empty(self):
        hpc = hp50g.HP50G()
        resp_stdout = hpc.hp_cmd('rhost CLEAR')
        self.assertTrue('Empty Stack' in resp_stdout)

    def test_hp_dir(self):
        """"Test change dir is successful"""
        hpc = hp50g.HP50G()
        wanted = ['HOME']

        hpdl = hpc.hp_dir(wanted)

        self.assertEqual(wanted, hpdl.dir_path_list)

    def test_hp_dir_2_levels(self):
        """"Test change dir is successful"""
        hpc = hp50g.HP50G()
        wanted = ['HOME', 'APPS']

        hpdl = hpc.hp_dir(wanted)

        self.assertEqual(wanted, hpdl.dir_path_list)

    def test_hp_dir_2_levels_dir_not_exist(self):
        """"Test change dir raises exception for unknown dir"""
        hpc = hp50g.HP50G()
        wanted = ['HOME', 'FOODDD']

        with self.assertRaises(ValueError):
            hpdl = hpc.hp_dir(wanted)

    def test_hp_get_tview(self):
        """"Test fetvhing TVIEW"""
        hpc = hp50g.HP50G()
        raw_tview = hpc.hp_get_tview()
        print(raw_tview)


if __name__ == '__main__':
    unittest.main()
