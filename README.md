hp50gshell
==========

Provides a simple shell for interacting with a HP 50g, from a Linux machine.
Current dev environment is Linux Mint 20.3, but should work with other Linux flavours.

Also supports (b)send/(b)get commands for sending binary/text files from PC 
to calulator and receiving binary/text files from calculator.

Still alpha stage, but mostly usable... bug reports are welcome!!

WARNING: Please backup your HP50g, just in case !!

This will create a backup called 'BUP42' on your SD card.

``` text
:3:BUP42 ARCHIVE
```

Instructions
------------

These instructions assume you have kermit installed and working.
See **docs/.kermrc** for a config example that should be copied to
your home directory.

Also copy  **docs/.hp50gshell.yaml** to your home directory
and modify the path to a working kermit (or wermit) executable.


``` text
python3 -m venv venv
. ./venv/bin/activate

pip install git+https://bitbucket.org/pymaximus/hp50gshell.git

hpshell.py

```
Once you see the banner and prompt for the shell, start kermit on
your HP50g and type **connect** at the hpshell prompt. It will take a few seconds
to connect and gather some file system information. When the prompt is back, you may issue
some commands. For example, type **sdir** for a sorted directory listing. 

Type **help** to see what is currently implemented.

Information
-----------

Uses Kermit as underlying transport but that will eventually be pluggable
.
Also supports issuing commands directly on calculator via **cmd**.

Here are a couple of screen shots, showing current capability.

![Screenshot](images/hp50gshell1.png)

![Screenshot](images/hp50gshell2.png)

Unix style commands. For example mkdir is shown here...

![Screenshot](images/mkdir.png)

Sending and getting files. In this case, some UserRPL programs.
Notice use can use command line completion for external files also. See how 
we can use **!cat y1** to issue shell commands also.

![Screenshot](images/send_get2.png)


Examining formulas and units using **cat** and **pcat**.

![Screenshot](images/formulas.png)

Executing a UserRPL program (CNTDWN) and seeing stack output

![Screenshot](images/cntdwn.png)

Copying objects to another directory

![Screenshot](images/cp_demo.png)

For example, see date on calculator, 

``` text
hp50g: ['HOME']>cmd 6 FIX DATE
1:             24.082019
```

Prompt show current path.

Also, nice screen output for **dir** command.

``` text
hp50g: ['HOME']>dir
TVIEW                    4865.5            List     52501.     0xcd15
ZZ                         13.0     Global Name     32260.     0x7e04
CCC                        15.5          String     61997.     0xf22d
CC                         13.0         Integer     47085.     0xb7ed
Z                          12.0     Global Name     32260.     0x7e04
P                          13.0            List      1226.      0x4ca
REMO5                     778.5         Program     11396.     0x2c84
REMO4                     815.0         Program     37228.     0x916c
REMO3                     867.5         Program     55594.     0xd92a
REMO2                     867.5         Program     24602.     0x601a
REMO                      599.0         Program     53887.     0xd27f
MYREPL.RPL               1535.0         Program     60528.     0xec70
V                         326.0            List     43264.     0xa900
FASTDIR                   167.5         Program     45909.     0xb355
TST                       153.5       Directory     10113.     0x2781
IOPAR                      37.5            List     19265.     0x4b41
FD2                        39.5         Program     42188.     0xa4cc
FD                         48.0         Program     16790.     0x4196
FASTDIR.RPL               215.0         Program     25605.     0x6405
TAS2                       15.0       Directory     24171.     0x5e6b
TAS                        14.0       Directory     24171.     0x5e6b
HPTREE                     75.0         Program     32880.     0x8070
RVIS                      290.0         Program     23246.     0x5ace
UNIXPATH                  150.5         Program     40337.     0x9d91
SEAL                       98.0       Directory     61283.     0xef63
FOO                       203.5       Directory     13937.     0x3671
PWD                        41.0         Program     18163.     0x46f3
HPTREE3.HPPRG             671.5         Program     60558.     0xec8e
A\piT                      48.0       Directory     30443.     0x76eb
Y1                         50.0         Program     46322.     0xb4f2
FIRST                      37.0         Program     37456.     0x9250
TEST                       84.0       Directory     62088.     0xf288
A2B                       543.0       Directory     51055.     0xc76f
APPS                     3025.0       Directory     37965.     0x944d
PTpar                      48.5    Library Data     16554.     0x40aa
TRIANGLES               28864.5       Directory     55733.     0xd9b5
EQUATION                 2540.0       Directory     64227.     0xfae3
UTILS                    4018.0       Directory     30514.     0x7732
BACKUP                    100.0       Directory     28007.     0x6d67
MYEQNS                    361.5       Directory      4275.     0x10b3
CASDIR                    527.0       Directory     20560.     0x5050
```



NOTES:
======

For HP50G Shell, make sure Flag 35 is clear for ASCII transfer and
NOT set (ticked means binary Transfer)

Flag: –35
 
Kermit I/O Data Format.
Clear: Objects transmitted in ASCII form.
Set: Objects transmitted in binary (memory image) form.

This can be sone by using **set_binary_xfer** or **set_binary_xfer** commands 
inside the shell.

Kermit init file on Fedora
--------------------------

``` text

hp50g: ['HOME']>! cat ~/.kermrc

set line /dev/ttyUSB0
set speed 9600
set carrier-watch off
set handshake none
set flow-control none
set parity none
set modem type direct
set prefixing all
set block 3
set send packet-length 80

```


Decompile
=========

Without extable.
----------------

``` text
hp50g: <U> ['HOME'] >decompile AC2 
hp50g: <U> ['HOME'] >cat tmpd 
"!NO CODE
!RPL
::
  PTR 26297
  PTR 26328
  # 1
  ::
    % 2.
    PTR 3045B
    % 3.14159265359
    PTR 303A7
  ;
;
@"

```

With extable2.lib
-----------------

``` text
hp50g: <T> ['HOME'] >decompile AC2 
hp50g: <T> ['HOME'] >cat tmpd 
"!NO CODE
!RPL
::
  CK1NOLASTWD
  CK&DISPATCH1
  BINT1
  ::
    %2
    %^
    %PI
    %*
  ;
;
@"

```


Dev Notes:
----------

To install into an existing python3 virtualenv, issue the following command.

``` text
pip install git+https://bitbucket.org/pymaximus/hp50gshell.git
```


C-Kermit Notes for HP50G:
-------------------------

The following has been observed using C-Kermit 9
on Linux Mint 20.3, when transferring files to
a HP50G calculator using USB connector.

Sending files to the HP50G using C-Kermit, may result in NAKs
being sent from the calculator (receiver) back to
C-Kermit (sender). It was observed that small files with data packets
around 50 bytes were ok, but larger ones (approx 100 byte data packets)
would fail with "Protocol Error" being displayed on the calculator.

According to section D-4 of 
"HP 50g / 49g+ / 48gII graphing calculator
advanced user’s reference manual", it mentions

```text
receive pacing

Controls whether receive pacing is used: a
nonzero real value enables pacing, while
zero disables it. Receive pacing sends an
XOFF signal when the receive buffer is
almost full, and sends an XON signal when
it can take more data again. Pacing is not
used for Kermit I/O, but is used for other
serial I/O transfers.

transmit pacing

Controls whether transmit pacing is used: a
nonzero real value enables pacing, while
zero disables it. Transmit pacing stops
transmission upon receipt of XOFF, and
resumes transmission upon receipt of XON.
Pacing is not used for Kermit I/O , but is
used for other serial I/O transfers.
```

So it appears HP50G Does not support XON/XOFF for kermit I/O.

Via some testing, and consultation with the C-Kermit maintainer, 
it was suggested that the HP50G may have an incoming packet length limit
but does not tell the file sender (C-Kermit in this case) about it in the
negotiation phase.

To avoid filling the small receive buffer of the HP50G, resulting in
repeated NAKs and Protocol Errors, we can tell C-Kermit (sender) to 
send smaller packets. 

Adding the following line to **~/.kermrc** appears to fix the issue.

```text
set send packet-length 80
```

