#
# Example kermit rdir
#
#
# This text
#

# { HOME EQUATION TRIGONOMETRY } 205651.
# TRIANGLE AREA 60.5 Algebraic 61717.
# HERONS RULE 26. Real Number 13207.
# C 16. Real Number 10767.
# SINE RULE B-C 53. Algebraic 5137.
# SINE RULE A-C 53. Algebraic 14762.
# SINE RULE A-B 53. Algebraic 52659.
# B 16. Real Number 11968.
# COS RULE C 81.5 Algebraic 23816.
# COS RULE B 81.5 Algebraic 12348.
# b 16. Real Number 51968.
# A 16. Real Number 37254.
# c 16. Real Number 43428.
# a 16. Real Number 51784.
# COS RULE A 81.5 Algebraic 7845.
#
# Becomes an HPDirList instance
#
#
# dir_path: HOME EQUATION TRIGONOMETRY, dir_size: 205651.,
# dirents: [
#   dname: TRIANGLE AREA, dsize: 60.5, dtype: Algebraic, checksum: 61717.,
#   dname: HERONS RULE, dsize: 26., dtype: Real Number, checksum: 13207.,
#   dname: C, dsize: 16., dtype: Real Number, checksum: 10767.,
#   dname: SINE RULE B-C, dsize: 53., dtype: Algebraic, checksum: 5137.,
#   dname: SINE RULE A-C, dsize: 53., dtype: Algebraic, checksum: 14762.,
#   dname: SINE RULE A-B, dsize: 53., dtype: Algebraic, checksum: 52659.,
#   dname: B, dsize: 16., dtype: Real Number, checksum: 11968.,
#   dname: COS RULE C, dsize: 81.5, dtype: Algebraic, checksum: 23816.,
#   dname: COS RULE B, dsize: 81.5, dtype: Algebraic, checksum: 12348.,
#   dname: b, dsize: 16., dtype: Real Number, checksum: 51968.,
#   dname: A, dsize: 16., dtype: Real Number, checksum: 37254.,
#   dname: c, dsize: 16., dtype: Real Number, checksum: 43428.,
#   dname: a, dsize: 16., dtype: Real Number, checksum: 51784.,
#   dname: COS RULE A, dsize: 81.5, dtype: Algebraic, checksum: 7845.]


import logging

# local modules
import hp50gshell
from hp50gshell import hputils


# Will hold a directory entry
class HPDirEntry:
    __slots__ = 'dname', 'dsize', 'dtype', 'checksum'

    def __init__(self, dname, dsize, dtype, checksum):
        self.dname = dname
        self.dsize = dsize
        self.dtype = dtype
        self.checksum = checksum

    def __repr__(self):
        return('\n  dname: %s, dsize: %s, dtype: %s, checksum: %s' %
               (self.dname, self.dsize, self.dtype, self.checksum))

    # for unit test assertEqual
    def __eq__(self, other):
        return [self.dname, self.dsize, self.dtype, self.checksum] == other


# will hold entire directory info
class HPDirList:

    def __init__(self):
        self.dir_path = None
        self.dir_path_list = []
        self.dir_size = 0
        self.dirents = []        # list of HPDirEntry objects

    def __str__(self):
        return('dir_path: %s\ndir_path_list: %s\ndir_size: %s\ndirents: %s' %
               (self.dir_path, self.dir_path_list,
                self.dir_size, self.dirents))

    def parse_rdir(self, rdir):
        logging.debug('---------')
        logging.debug(rdir)
        logging.debug('---------')
        try:
            logging.debug('rdir\n%s' % rdir)

            # Fetch directory path between brackets eg: { HOME APPS }
            dir_details = hputils.get_dir_details(rdir)
            self.dir_path = dir_details['path']
            self.dir_path_list = dir_details['path'].split()
            self.dir_size = dir_details['size']

            logging.debug('------------------->')
            logging.debug(self.dir_path)
            logging.debug(self.dir_size)
            logging.debug('<------------------')

            dirent_details = hputils.get_dirent_details(rdir)
            logging.debug('dirent_details = %s' % dirent_details)
            for dd in dirent_details:
                hpde = HPDirEntry(dd['name'],
                                  dd['size'],
                                  dd['type'],
                                  dd['checksum'])

                self.dirents.append(hpde)

        except Exception as e:
            logging.exception("Error in parse_dir")
            raise e
