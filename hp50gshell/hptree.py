# standard modules
import unittest
import cProfile
import pdb
import logging

# local modules
import mylog
import hp50g
import hpdirlist



def visit(path, hp, indent):
    # fetch dir entries
    logging.warning(path)
    hpdl = hp.hp_dir(path)
    logging.warning(hpdl.dirents)

    for entry in hpdl.dirents:
        logging.warning('path = %s, entry: %s' % (path, entry))
        if entry.dtype == 'Directory':
            print('  ' * indent, entry.dname)
            # recursive
            next_path = path + [entry.dname]
            logging.warning('next_path %s' % next_path)
            visit(next_path, hp, indent + 2)
        else:
            print('  ' * indent, entry.dname)


def do_main():
    hp = hp50g.HP50G()
    # visit filesystem on calulator, starting from { HOME }
    path = ['HOME']
    visit(path, hp, 0)


if __name__ == '__main__':

    #cProfile.run('do_main()')
    do_main()
