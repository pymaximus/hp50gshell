# setup logging

# standard modules
import logging
logging.basicConfig(
    filename="/tmp/hpshell.log",
    level=logging.DEBUG,
    format="%(asctime)s:%(levelname)s:%(module)s:%(funcName)s:%(message)s"
    )
