# standard module
import re
import logging

# 3rd party modules

# local modules

import hp50gshell

# NOTE: calculator may put  line feed in the middle
# of return data (eg: long directory or file names). To fix this, the
# regex should scan the entire rdir string
# and NOT be line oriented.

#
# compile regexes
#

# { HOME FOO BAR } 12345.
re_dir_details = re.compile(r'\{\s*(?P<path>.*?)\s*\}\s*(?P<size>[0-9\.]+)')

# TRIANGLE AREA 60.5 Algebraic 61717.
# HERONS RULE 26. Real Number 13207.
# C 16. Real Number 10767.
# SINE RULE B-C 53. Algebraic 5137.
# SINE RULE A-C 53. Algebraic 14762.
# SINE RULE A-B 53. Algebraic 52659.
# B 16. Real Number 11968.
# COS RULE C 81.5 Algebraic 23816.
# COS RULE B 81.5 Algebraic 12348.
# b 16. Real Number 51968.
# A 16. Real Number 37254.
# c 16. Real Number 43428.
# a 16. Real Number 51784.
# COS RULE A 81.5 Algebraic 7845.

# FIXME: Assumption. Directory entry names start with [A_Za-z]
re_dirent_details = re.compile(r'(?P<name>[A-Za-z].*?)\s+(?P<size>[0-9\.]+)\s+(?P<type>.+?)\s+(?P<checksum>[0-9\.]+)')


# Example PATH output
# { HOME FOO BAR }
re_path_details = re.compile(r'\{\s*(?P<path>.*?)\s*\}')


# This
# { HOME EQUATION TRIGONOMETRY } 205651.
# becomes
# {'path': 'HOME EQUATION TRIGONOMETRY', 'size': '205651.'}
#
def get_dir_details(text):
    logging.debug('text: [%s]' % text)
    # remove linefeed FIXME: fails when \n separates 2 directory names that wrap line
    # { HOME EQUATION ELECTRICAL SPEEDABC instead of { HOME EQUATION ELECTRICAL SPEED ABC }
    # So how shall we interpret { HOME EQUATION ELECTRICAL SPEED\nABC } from rdir reponse ???
    text = text.replace('\n', ' ')
    logging.debug('text without linefeed [%s]' % text)
    m = re_dir_details.search(text)
    if m:
        logging.debug('m = %s' % m)
        return m.groupdict()
    else:
        logging.error('Could not find { PATH } in text [%s]' % text)


# This
# ' BIG TRIANGLES 28864.5 Directory 55733.'
# becomes
# {'name': 'BIG TRIANGLES', 'size': '28864.5', 'type': 'Directory', 'checksum': '55733.'}
#
# FIXME: this is not robust, bit will do for now
def get_dirent_details(text):
    """Parse entire rdir output, ignore directory line and just find """
    logging.debug('text: [%s]' % text)

    try:
        # remove linefeed
        text = text.replace('\n', '')
        logging.debug('dirent text without linefeed [%s]' % text)

        if '}' in text:
            # remove directory line, gotta improve this.
            # assume no } inside dir path element.
            # Good: { HOME APP} 1234.
            # Bad:  { HOME APP}22 FOO }
            # also assumes that dire size is had no digit after the decimal point
            # Good: 12345.
            # Bad: 12345.5
            text = text.split("}", 1)[1]
            text = text.split(".", 1)[1]

        logging.debug('text after removing dir section [%s]' % text)

        m_iter = re_dirent_details.finditer(text)
        if m_iter:
            logging.debug('dirent = %s' % m_iter)
            dirents = [m.groupdict() for m in m_iter]
            logging.debug('dirents = %s' % dirents)

            return dirents

        else:
            logging.error('Could not find dirent details in text [%s]' % text)

    except Exception as e:
        logging.exception('Error parsing text in get_dir_details()')
        raise e


# This
# { HOME EQUATION TRIGONOMETRY }
# becomes
# {'path': 'HOME EQUATION TRIGONOMETRY'}
#
def get_path_details(text):
    logging.debug('text: [%s]' % text)
    # remove linefeed FIXME: fails when \n separates 2 directory names that wrap line
    # { HOME EQUATION ELECTRICAL SPEEDABC instead of { HOME EQUATION ELECTRICAL SPEED ABC }
    # So how shall we interpret { HOME EQUATION ELECTRICAL SPEED\nABC } from rdir reponse ???
    text = text.replace('\n', '')
    logging.debug('text without linefeed [%s]' % text)
    m = re_path_details.search(text)
    if m:
        logging.debug('m = %s' % m)
        return m.groupdict()
    else:
        logging.error('Could not find { PATH } in text [%s]' % text)


# Example kermit rdir
#
# { HOME } 205642.
# TEST 57. Directory 59130.
# A2B 422. Directory 34392.
# APPS 2784.5 Directory 62791.
# PTpar 48.5 Library Data 16554.
# TRIANGLES 28864.5 Directory 55733.
# EQUATION 2024. Directory 4503.
# IOPAR 29.5 List 45664.
# UTILS 3989. Directory 18236.
# BACKUP 100. Directory 28007.
# MYEQNS 361.5 Directory 4275.
# CASDIR 527. Directory 20560.


def pc_dir_from_calc_dir(calc_dir):
    """
    ['HOME'] -> '/'
    ['HOME','APPS'] -> '/APPS'
    ['HOME','APPS','PLANK'] -> '/APPS/PLANK'
    """
    return '/' + '/'.join(calc_dir[1:])


def calc_dir_from_pc_dir(pc_dir):
    """
    '/' -> ['HOME']
    '/APPS' -> ['HOME','APPS']
    '/APPS/PLANK' -> ['HOME','APPS','PLANK']
    """
    if pc_dir == '/':
        return ['HOME']
    else:
        return ['HOME'] + list(filter(None, pc_dir.split('/')))


re_tview_details = re.compile(r'\:(?P<name>/.*?)\:\s+(?P<type>[0-9]+\.)\s+')

# %%HP: T(3)A(R)F(.);
# { :/HOME/HPTREE: 8. :/HOME/RVIS:
# 8. :/HOME/UNIXPATH: 8.
# :/HOME/HPTREE2.HPPRG: 8.
# :/HOME/VISITOR.TXT: 8.
# :/HOME/CNTDWN~1.HPP: 2.
# :/HOME/A\piT: 15.
# :/HOME/A\piT/FRED\pi\Gt\Gl: 15.
# :/HOME/A\piT/A: 0. :/HOME/Y1: 8.
# :/HOME/FIRST: 8. :/HOME/ACCU.HP:
# 8. :/HOME/TEST: 15.
# :/HOME/TEST/ACIR: 8. :/HOME/A2B:
# 15. :/HOME/A2B/CNTDWN: 8.
# :/HOME/A2B/UP: 8.
# :/HOME/A2B/ASCIIBIN.49: 8.
# :/HOME/APPS: 15. :/HOME/APPS/UP:
# 8. :/HOME/APPS/CONV: 15.
# :/HOME/APPS/CONV/UP: 8.
# :/HOME/APPS/CONV/MPG2LPER100KM:
# 8. :/HOME/APPS/PLANK: 15.
# :/HOME/APPS/PLANK/UP: 8.
# :/HOME/APPS/PLANK/PPAR: 5.
# :/HOME/APPS/PLANK/SPLANK.RPL: 8.
# :/HOME/APPS/PLANK/DPLANK.RPL: 8.
# :/HOME/PTpar: 26.
# :/HOME/TRIANGLES: 15.
# :/HOME/TRIANGLES/UP: 8.
# :/HOME/TRIANGLES/PPAR: 5.
# :/HOME/TRIANGLES/TRNGL.HP: 8.
# :/HOME/EQUATION: 15.
# :/HOME/EQUATION/RF: 15.
# :/HOME/EQUATION/RF/Vref: 0.
# :/HOME/UTILS/DEMO/SQUARE: 8.
# :/HOME/UTILS/D2: 8.
# :/HOME/UTILS/A2B: 8.
# :/HOME/BACKUP: 15.
# :/HOME/BACKUP/PPAR: 5.
# :/HOME/MYEQNS: 15.
# :/HOME/MYEQNS/EQ: 9.
# :/HOME/MYEQNS/RSERIES: 9.
# :/HOME/MYEQNS/RPARALLEL: 9.
# :/HOME/MYEQNS/ZPAR: 5.
# :/HOME/MYEQNS/PPAR: 5.
# :/HOME/MYEQNS/CST: 5.
# :/HOME/CASDIR: 15.
# :/HOME/CASDIR/SYSTEM: 5.
# :/HOME/CASDIR/ENVSTACK: 5.
# :/HOME/CASDIR/PERIOD: 9.
# :/HOME/CASDIR/EPS: 0.
# :/HOME/CASDIR/MODULO: 28.
# :/HOME/CASDIR/PRIMIT: 9.
# :/HOME/CASDIR/CASINFO: 11.
# :/HOME/CASDIR/REALASSUME: 5.
# :/HOME/CASDIR/VX: 6. }


def get_tview_details(text):
    logging.debug('text: [%s]' % text)
    text = text.replace('\n', ' ')
    logging.debug('text without linefeed [%s]' % text)

    m_iter = re_tview_details.finditer(text)
    if m_iter:
        logging.debug('tview = %s' % m_iter)
        tview_entries = [m.groupdict() for m in m_iter]
        logging.debug('dirents = %s' % tview_entries)
        return tview_entries
    else:
        logging.error('Could not find tview details in text [%s]' % text)
