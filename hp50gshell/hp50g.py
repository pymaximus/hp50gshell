#!/usr/bin/env python3
#
# Provide API for communication with HP50G.
#

# standard modules
# import logging
# logging.basicConfig(
#     filename="hp50g.log",
#     level=logging.DEBUG,
#     format="%(asctime)s:%(levelname)s:%(module)s:%(funcName)s:%(message)s"
#     )


import subprocess

import io
import logging
import stat
import time

# local modules
import hp50gshell
from hp50gshell import hpdirlist


class HP50G:

    def __init__(self, kermit_path='/usr/bin/kermit'):
        logging.debug('HP50G called')
        self.cwd = None
        self.hpdirentries = []
        self.kermit_path = kermit_path

        # must go to HOME dir first
        self.hp_home()
        # and 3 TRANSIO
        self.hp_transio(3)

    def XXhp_stat_from_dir_entry(self, entry):
        logging.debug('entry: %s ' % entry)

        # keep it simple for now
        if 'Directory' in entry:
            mode = 0o755
            return dict(
                st_mode=(stat.S_IFDIR | mode),
                st_nlink=2,
                st_size=0,
                st_ctime=time(),
                st_mtime=time(),
                st_atime=time())

        else:
            mode = 0o755
            return dict(
                st_mode=(stat.S_IFREG | mode),
                st_nlink=1,
                st_size=0,
                st_ctime=time(),
                st_mtime=time(),
                st_atime=time())

    # Naive: needs work
    def hp_parse_stack_string(self, stack_string):
        logging.debug('stack_string:\n%s' % stack_string)
        stack = []
        buf = io.StringIO(stack_string)  # to allow readline
        for stack_line in buf.readlines():
            if not stack_line.isspace():
                logging.debug('stack line [%s]' % stack_line)
                fields = stack_line.split()
                logging.debug(fields)

                stack.append(fields[1])

        # reverse list to match stack index
        stack.reverse()
        logging.debug(stack)
        return stack

    def hp_cmd(self, cmd):
        try:
            logging.debug(f'[{cmd}]')
            proc = subprocess.Popen([self.kermit_path, '-C', '%s' % cmd],
                                    stdin=subprocess.PIPE,
                                    stdout=subprocess.PIPE,)

            cmd_resp_stdout = proc.communicate()[0].decode('iso-8859-1')
            logging.debug('cmd stdout:[%s]' % cmd_resp_stdout)

            # quit command sequence
            proc = subprocess.Popen([self.kermit_path, '-C', 'quit'],
                                    stdin=subprocess.PIPE,
                                    stdout=subprocess.PIPE)

            quit_resp_stdout = proc.communicate()[0].decode('iso-8859-1')
            logging.debug('quit stdout [%s]' % quit_resp_stdout)
            # only want response from initial command
            return cmd_resp_stdout

        except Exception as e:
            logging.exception('Error during hp_cmd')
            raise e

    def hp_home(self):
        cmd = 'remote host { HOME }'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

    def hp_transio(self, mode):
        cmd = 'remote host %s TRANSIO' % mode
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

    def hp_date(self):
        cmd = 'remote host 6 FIX DATE'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

    def hp_kermit_cmd_status(self):
        cmd = 'remote host 6 FIX DATE'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

    def hp_updir(self):
        cmd = 'remote host UPDIR'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

    def hp_attr(self, hpdirent):
        fields = hpdirent.split()
        entry_type = fields[2]
        if entry_type == 'Directory':
            return stat.S_IFDIR
        else:
            return stat.S_IFREG

    def hp_dir(self, dir_list):
        """Execute rdir on specified directory"""
        logging.debug('dir_list = %s' % dir_list)
        hppath = ' '.join(dir_list)
        logging.debug('hppath = %s' % hppath)
        logging.debug(hppath)

        cmd = 'remote host { %s }' % hppath
        cmd = cmd.replace('\\', '\\\\')

        resp_stdout = self.hp_cmd(cmd)
        logging.debug('\n' + resp_stdout)

        # execute rdir and save results
        cmd = 'rdir'
        resp_stdout = self.hp_cmd(cmd).strip()
        logging.debug('\n[' + resp_stdout + ']')

        # fetch container and parse
        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(resp_stdout)
        logging.debug(hpdl)

        # assert chdir was successful using PATH
        # wanted dir and actual dir must match,
        #  otherwise something broken
        if dir_list != hpdl.dir_path_list:
            logging.error('dir_list != hpdl.dir_path_list')
            raise ValueError('hp_dir: dir_list != hpdl.dir_path_list')

        return hpdl

    def hp_cd_home():
        proc = subprocess.Popen([self.kermit_path, '-C', "remote host { HOME }, quit"],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE)

        proc.communicate()[0].decode('iso-8859-1')

    def hp_path(self):
        """Get path of current directory"""
        cmd = 'remote host PATH'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('hp_path: %s' % resp_stdout)

        path_list = resp_stdout.split()
        return path_list

    def hp_get_tview_ver1(self):
        """Get TVIEW from calulator"""
        cmd = 'set file type text'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('response: %s' % resp_stdout)

        cmd = 'rhost 3 TRANSIO'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('response: %s' % resp_stdout)

        cmd = 'rhost HOME'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('response: %s' % resp_stdout)

        cmd = 'GET TVIEW'
        resp_stdout = self.hp_cmd(cmd)
        logging.debug('response: %s' % resp_stdout)

        path_list = resp_stdout.split()
        return path_list

    def hp_get_tview(self):
        proc = subprocess.Popen([self.kermit_path, '-I', '-T', '-G', 'TVIEW'],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')
        return resp_stdout
