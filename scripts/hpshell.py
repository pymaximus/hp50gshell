#!/usr/bin/env python3
"""Simple shell for my HP 50G"""
#
# Simple shell allowing terminal based command/response interaction with a HP 50G.
# Currently uses kermit as underlying transport. Supports text and binary transfers.
#
# Copyright (C) 2019, Frank Singleton, b17flyboy@gmail.com
#

# system modules
import logging
import os
import subprocess
import datetime
import time
import yaml

# 3rd part modules
import cmd2
from cmd2 import Cmd, ansi

# local modules
import hp50gshell
from hp50gshell import hp50g
from hp50gshell import hputils
from hp50gshell import hpdirlist
from hp50gshell import hptrans

# GIT Meta

_VERSION = '$Id 0200fa5f039de410a7e61ad9e9b3696d550d4175 $'

# shameless plug !!

_BANNER = r'''
  _    _ _____    _____  ___   _____    _____ _    _ ______ _      _
 | |  | |  __ \  | ____|/ _ \ / ____|  / ____| |  | |  ____| |    | |
 | |__| | |__) | | |__ | | | | |  __  | (___ | |__| | |__  | |    | |
 |  __  |  ___/  |___ \| | | | | |_ |  \___ \|  __  |  __| | |    | |
 | |  | | |       ___) | |_| | |__| |  ____) | |  | | |____| |____| |____
 |_|  |_|_|      |____/ \___/ \_____| |_____/|_|  |_|______|______|______|

      Copyright (C) Frank Singleton b17flyboy@gmail.com

'''

# for reading config file
def read_yaml(file_path):
    with open(file_path, "r") as f:
        return yaml.safe_load(f)


class ShellPrompt:
    """Attributes that make up the shell prompt"""
    prompt_prefix = 'NC:>'
    xfer_mode = 'U'
    local_dir = os.getcwd()
    cwd = []

    @classmethod
    def get(cls):
        return ansi.style('{} <{}> {} {} >'.format(cls.prompt_prefix,
                                                   cls.xfer_mode,
                                                   cls.local_dir,
                                                   [hptrans.pretty_print(x) for x in cls.cwd]),
                          fg=ansi.Fg.LIGHT_YELLOW)


# Nice, but quite slow, due to kermit overhead
def visit(path, hpc, indent):
    """Recursive function to get tree of variables from HP 50G"""
    # recursively fetch dir entries from calulator
    # TODO: use HPTREE on calculator instead
    logging.debug(path)
    hpdl = hpc.hp_dir(path)
    logging.warning(hpdl.dirents)

    for entry in hpdl.dirents:
        logging.debug('path = %s, entry: %s', path, entry)
        if entry.dtype == 'Directory':
            print('  ' * indent, entry.dname)
            # recursive
            next_path = path + [entry.dname]
            logging.debug('next_path %s', next_path)
            visit(next_path, hpc, indent + 2)
        else:
            print('  ' * indent, entry.dname)


class HPShell(Cmd):
    """HPShell - encapsulates required methods to provide useful interactive shell"""
    prompt = ShellPrompt.get()
    intro = "Welcome! Type ? to list commands. Please start Kermit on HP 50g"

    # command categories
    CMD_MAIN_COMMANDS = 'Main Commands'
    CMD_CAT_KERMIT = 'Kermit Commands'
    CMD_FILE_TRANSFER = 'File Transfer'
    CMD_FLAG_COMMANDS = 'Flag Commands'
    CMD_RAW_COMMAND = 'Raw Command'
    CMD_DEV_COMMAND = 'Developer Command'

    # Default path for kermit read from config file
    config = read_yaml(os.path.expanduser('~/.hp50gshell.yaml'))
    logging.debug(config)

    KERMIT_PROG = config['kermit']['path']

    def __init__(self):
        super().__init__(include_ipy=False)
        # Should ANSI color output be allowed
        #self.allow_ansi = ansi.ANSI_TERMINAL
        self.prompt = ShellPrompt.get()    # default prompt until connected
        self.timing = True      # show elapsed time for commands
        self.set_window_title('HP 50G Shell')
        self.poutput(ansi.style(_BANNER, fg=ansi.Fg.LIGHT_GREEN))
        self.hpc = None          # handle to hp calculator API functions
        self.hpdl = None         # local structure reflecting current directory on calculator
        self.kermit_path = self.KERMIT_PROG  # default

        self.add_settable(cmd2.Settable('kermit_path',
                                        str,
                                        '(c)kermit path', self,
                                        onchange_cb=self._onchange_kermit_path
        ))
        for alias_name, alias_desc in self.config.get("alias", {}).items():
            self.do_alias("create " + alias_name + " " + alias_desc)

    def _onchange_kermit_path(self, param_name, oldval, newval):
        self.kermit_path = newval

    def do_banner(self, _inp):
        """Display banner for shell"""
        self.poutput(ansi.style(_BANNER, fg=ansi.Fg.LIGHT_GREEN))

    # FIXME: Make faster, use HPTREE on calculator, cf: server side query
    def do_tree(self, _inp):
        """Linux 'tree' command, but slower!!"""
        visit(['HOME'], self.hpc, 0)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_vars(self, _inp):
        """Issue VARS command on calculator"""
        cmd = 'rhost VARS'
        resp_stdout = self.hpc.hp_cmd(cmd)
        self.poutput(resp_stdout)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_settime(self, _inp):
        """Set time on calculator"""
        ltime = datetime.datetime.now().strftime("%H.%M%S%f")[0:9].encode()
        cmd = rf'{ltime} \141TIME'  # raw formatted strings, awesome!!
        self.do_cmd(cmd, False)


    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_lcd(self, inp):
        """Local change directory"""
        os.chdir(inp)
        ShellPrompt.local_dir = os.getcwd()
        self.prompt = ShellPrompt.get()

    complete_lcd = cmd2.Cmd.path_complete

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_cat(self, inp, pp=False):
        """Show text verion of a VAR (object)"""
        if inp.strip('"') not in [x.dname for x in self.hpdl.dirents]:   # where should this be, in completer?
            self.perror('Please select an existing variable', apply_style=True)
            return

        # handle case where there are spaces in the names
        # make a copy into 'tmpv' and show that instead
        if ' ' in inp:
            self.do_cmd('''{} S~N RCL 'tmpv' STO'''.format(inp), display=False)
            inp = 'tmpv'
        else:
            # dont need " around vars that dont have spaces in them
            inp = inp.strip('"')

        proc_cmd = [self.kermit_path, '-P', '-G', '%s' % inp]
        logging.debug('proc_cmd = {}'.format(proc_cmd))

        proc = subprocess.Popen(proc_cmd,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')

        if pp:
            # pretty print requested
            resp_stdout = hptrans.pretty_print(resp_stdout)

        # display without the %%HP: T(3)A(R)F(.); line
        for line in resp_stdout.splitlines():
            if not line.startswith('%%HP: T'):
                self.poutput(line)

    # completion for show command, dont allow show directory
    def complete_cat(self, text, _line, _begidx, _endidx):
        """Completer for do_show()"""
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype != 'Directory']
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_DEV_COMMAND)
    def do_decompile(self, inp):
        """Decompile object and stote in 'tmpd' VAR. Use cat tmpd to see contents"""
        if inp.strip('"') not in [x.dname for x in self.hpdl.dirents]:   # where should this be, in completer?
            self.perror('Please select an existing variable', apply_style=True)
            return

        # handle case where there are spaces in the names
        # make a copy into 'tmpv' and show that instead
        if ' ' in inp:
            self.do_cmd('''{} S~N RCL 'tmpv' STO'''.format(inp), display=False)
            inp = 'tmpv'
        else:
            # dont need " around vars that dont have spaces in them
            inp = inp.strip('"')

        # 'var' RCL ->S2 'tmpd' STO
        self.do_cmd(r"'tmpd' PURGE '{}' RCL \141S2 'tmpd' STO".format(inp), False)

    def complete_decompile(self, text, _line, _begidx, _endidx):
        """Completer for do_decompile()"""
        # dont allow decompile directory
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype != 'Directory']
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_FILE_TRANSFER)
    def do_get(self, fname):
        logging.debug('do_get: fname = [{%s}]', fname)
        """GET var from calculator and save to local file.\nThis may be a simple variable or a directory for example."""
        proc_cmd = [self.kermit_path, '-g', '%s' % fname]
        logging.debug('proc_cmd = {}'.format(proc_cmd))

        proc = subprocess.Popen(proc_cmd,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')
        self.poutput(resp_stdout)

    def complete_get(self, text, _line, _begidx, _endidx):
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]
        return completions

    @cmd2.with_category(CMD_FILE_TRANSFER)
    def do_bget(self, fname):
        """Get BINARY file from calculator"""
        # Set binary transfer on calc for this to work.
        self.do_set_binary_xfer(None)
        logging.debug('do_bget: fname = [{%s}]', fname)
        """GET var from calculator and save to local file.\nThis may be a simple variable or a directory for example."""
        proc_cmd = [self.kermit_path, '-i', '-g', '%s' % fname]
        logging.debug('proc_cmd = {}'.format(proc_cmd))

        proc = subprocess.Popen(proc_cmd,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')
        self.poutput(resp_stdout)

    def complete_bget(self, text, _line, _begidx, _endidx):
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]
        return completions

    @cmd2.with_category(CMD_FILE_TRANSFER)
    def do_send(self, fname):
        """Send TEXT file to calculator"""
        proc = subprocess.Popen([self.kermit_path, '-s', '%s' % fname],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')
        self.poutput(resp_stdout)

    def complete_send(self, text, line, begidx, endidx):
        """send command requires local file name completion"""
        index_dict = \
            {
                # Tab-complete using path_complete function at index 1 in command line
                1: self.path_complete
            }

        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict)

    @cmd2.with_category(CMD_FILE_TRANSFER)
    def do_bsend(self, fname):
        """Send BINARY file to calculator"""
        proc = subprocess.Popen([self.kermit_path, '-i', '-s', '%s' % fname],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,)

        resp_stdout = proc.communicate()[0].decode('iso-8859-1')
        self.poutput(resp_stdout)

    def complete_bsend(self, text, line, begidx, endidx):
        """bsend command requires local file name completion"""
        index_dict = \
            {
                # Tab-complete using path_complete function at index 1 in command line
                1: self.path_complete
            }

        return self.index_based_complete(text, line, begidx, endidx, index_dict=index_dict)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_mkdir(self, dname):
        """Make new directory on calculator"""
        # allow 'mkdir W\Ga' instead of having to type 'mkdir W\\Ga' to make Wα
        dname = dname.replace('\\', '\\\\')
        self.do_cmd("'{}' CRDIR".format(dname), False)
        self.do_refresh(None)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    @cmd2.with_argument_list
    def do_cp(self, arglist):
        """Copy source (1st arg) object to dest (2nd arg) object"""
        # Only allow copying to existing destination if destination is a
        # directory.

        # fetch list of existing directories in cwd
        dirs = [x.dname for x in self.hpdl.dirents if x.dtype == 'Directory']
        if arglist[1] in dirs:
            # copy object to existing directory
            self._cp_src_to_existing_dir(arglist[0], arglist[1])
            self.do_refresh(None)
        else:
            # destination does not exist, just being safe
            blacklist = [x.dname for x in self.hpdl.dirents]
            if arglist[1] in blacklist:
                self.perror('Destination object already exists.', apply_style=True)
                return

            self.do_cmd("'{}' RCL '{}' STO".format(arglist[0], arglist[1]), False)
            self.do_refresh(None)

    def _cp_src_to_existing_dir(self, src, dest):
        """Copy src to existing dest dir in current directory"""
        # cmd PUSH 'src' RCL 'dest' EVAL 'src' STO POP
        self.do_cmd("PUSH '{}' RCL '{}' EVAL '{}' STO POP".format(src, dest, src), False)

    def _cp_src_to_existing_dir_rm_src(self, src, dest):
        """Copy src to existing dest dir in current directory, then delete src"""
        self.do_cmd("PUSH '{}' RCL '{}' EVAL '{}' STO POP '{}' PURGE".format(src, dest, src, src), False)

    def _cp_src_to_existing_dir_rm_src_dir(self, src, dest):
        """Copy src to existing dest dir in current directory, then delete src dir"""
        self.do_cmd("PUSH '{}' RCL '{}' EVAL '{}' STO POP '{}' PGDIR".format(src, dest, src, src), False)

    def complete_cp(self, text, _line, _begidx, _endidx):
        """Completer for copy"""
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    @cmd2.with_argument_list
    def do_mv(self, arglist):
        """Move source (1st arg) object to dest (2nd arg) object"""
        # Only allow moving to existing destination if destination is a
        # directory.

        # fetch list of existing directories in cwd
        dirs = [x.dname for x in self.hpdl.dirents if x.dtype == 'Directory']
        if arglist[1] in dirs:
            # dst is a dir, move object to existing directory
            if arglist[0] in dirs:
                # src is a dir
                self._cp_src_to_existing_dir_rm_src_dir(arglist[0], arglist[1])
                self.do_refresh(None)
            else:
                # src is not a dir
                self._cp_src_to_existing_dir_rm_src(arglist[0], arglist[1])
                self.do_refresh(None)
        else:
            # dest is not a directory
            # check destination does not exist, just being safe
            blacklist = [x.dname for x in self.hpdl.dirents]
            if arglist[1] in blacklist:
                self.perror('Destination object already exists.', apply_style=True)
                return

            # so, dest does not exist
            self.do_cmd("'{}' RCL '{}' STO '{}' PURGE".format(arglist[0], arglist[1], arglist[0]), False)
            self.do_refresh(None)

    def complete_mv(self, text, _line, _begidx, _endidx):
        """Completer for mv"""
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_rmdir(self, dname):
        """Delete directory on calculator"""
        self.do_cmd("'{}' PGDIR".format(dname), False)
        self.do_refresh(None)

    def complete_rmdir(self, text, _line, _begidx, _endidx):
        """Completer for do_rmdir()"""
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype == 'Directory']
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_FLAG_COMMANDS)
    def do_set_binary_xfer(self, _):
        """Set BINARY transfer on calculator (Set Flag -35)"""
        self.do_cmd('-35 SF', False)
        ShellPrompt.xfer_mode = 'B'
        self.prompt = ShellPrompt.get()

    @cmd2.with_category(CMD_FLAG_COMMANDS)
    def do_set_text_xfer(self, _):
        """Set TEXT transfer on calculator (Clear Flag -35)"""
        self.do_cmd('-35 CF', False)
        ShellPrompt.xfer_mode = 'T'
        self.prompt = ShellPrompt.get()

    @cmd2.with_category(CMD_FLAG_COMMANDS)
    def do_set_flag(self, inp):
        """Set System Flag on calculator"""
        self.do_cmd('{} SF'.format(inp), False)

    @cmd2.with_category(CMD_FLAG_COMMANDS)
    def do_clear_flag(self, inp):
        """Clear System Flag on calculator"""
        self.do_cmd('{} CF'.format(inp), False)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_rm(self, fname):
        """Delete file on calculator"""
        self.do_cmd("'{}' PURGE".format(fname), False)
        self.do_refresh(None)

    def complete_rm(self, text, _line, _begidx, _endidx):
        """Completer for do_rm()"""
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype != 'Directory']
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]

        return completions

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_version(self, _):
        """Display Git version"""
        self.poutput(_VERSION)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_pcat(self, inp):
        """cat with pretty print output"""
        self.do_cat(inp, pp=True)

    # completion for show command.
    def complete_pcat(self, text, _line, _begidx, _endidx):
        """Completer for do_pshow()"""
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]
        return completions

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_exit(self, _inp):
        """Exit hook"""
        print("Bye")
        return True

    def help_exit(self):
        """Helper for do_exit()"""
        print('exit the application. Shorthand: x q Ctrl-D.')

    @cmd2.with_category(CMD_CAT_KERMIT)
    def do_connect(self, inp):
        """Make connection to HP50G. Make sure Kermit is running on calculator first."""
        self.hpc = hp50g.HP50G(self.kermit_path)
        # issue dir command on calc and update local copy in hpdl
        self.hpdl = self.hpc.hp_dir(['HOME'])
        logging.debug('do_connect \n%s', self.hpdl)
        # update prompt after intial connection is made
        ShellPrompt.prompt_prefix = 'hp50g:'
        ShellPrompt.cwd = self.hpdl.dir_path_list
        self.prompt = ShellPrompt.get()

    # completion for cmd command.
    def complete_cmd(self, text, _line, _begidx, _endidx):
        """Completer for do_cmd()"""
        candidates = [x.dname for x in self.hpdl.dirents]
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]
        return completions

    @cmd2.with_category(CMD_CAT_KERMIT)
    def do_rdir(self, _path):
        """Execute Kermit RDIR"""
        cmd = 'rdir'
        resp_stdout = self.hpc.hp_cmd(cmd)
        print(resp_stdout)

    def display_directory(self, hpdl, pp=True, sort_dir=False):
        """Display formatted directory listing"""
        # TODO: add pp support for VAR names, perhaps another column

        if pp:
            format_string = '{:30} {:30} {:10.1f} {:>25} {:>10} {:>10}'
        else:
            format_string = '{:30} {:10.1f} {:>25} {:>10} {:>10}'

        if sort_dir:
            # sort by dname, as lowercase
            entries = sorted(hpdl.dirents, key=lambda x: x.dname.lower(), reverse=False)
        else:
            # otherwise sorted as they appear from rdir
            entries = hpdl.dirents

        for entry in entries:

            # common field
            name_field = entry.dname   # may be colorized below
            dsize_field = float(entry.dsize)
            dtype_field = entry.dtype
            checksum_field = entry.checksum
            hex_checksum_field = hex(int(float(entry.checksum)))

            if entry.dtype == 'Directory':
                name_field = ansi.style(name_field, fg=ansi.Fg.GREEN)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.GREEN)
            elif entry.dtype == 'Program':
                name_field = ansi.style(name_field, fg=ansi.Fg.BLUE)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.BLUE)
            elif entry.dtype == 'List':
                name_field = ansi.style(name_field, fg=ansi.Fg.RED)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.RED)
            elif entry.dtype == 'Algebraic':
                name_field = ansi.style(name_field, fg=ansi.Fg.YELLOW)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.YELLOW)
            elif entry.dtype == 'Integer':
                name_field = ansi.style(name_field, fg=ansi.Fg.CYAN)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.CYAN)
            elif entry.dtype == 'Library Data':
                name_field = ansi.style(name_field, fg=ansi.Fg.MAGENTA)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.MAGENTA)
            elif entry.dtype == 'Global Name':
                name_field = ansi.style(name_field, fg=ansi.Fg.LIGHT_MAGENTA,)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.LIGHT_MAGENTA)
            elif entry.dtype == 'String':
                name_field = ansi.style(name_field, fg=ansi.Fg.LIGHT_MAGENTA)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.LIGHT_MAGENTA)
            elif entry.dtype == 'Graphic':
                name_field = ansi.style(name_field, fg=ansi.Fg.LIGHT_CYAN)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.LIGHT_CYAN)
            elif entry.dtype == 'Real Number':
                name_field = ansi.style(name_field, fg=ansi.Fg.LIGHT_GREEN)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.LIGHT_GREEN)
            else:
                name_field = ansi.style(name_field, fg=ansi.Fg.WHITE)
                dtype_field = ansi.style(dtype_field, fg=ansi.Fg.WHITE)

            # extra column if pretty print
            if pp:
                print(format_string.format(name_field, hptrans.pretty_print(name_field),
                                           dsize_field,
                                           dtype_field,
                                           checksum_field,
                                           hex_checksum_field))
            else:
                print(format_string.format(name_field,
                                           dsize_field,
                                           dtype_field,
                                           checksum_field,
                                           hex_checksum_field))

        ShellPrompt.cwd = self.hpdl.dir_path_list
        self.prompt = ShellPrompt.get()

    def display_directory_plain(self, hpdl):
        """Display directory structure without ANSI highlighting"""
        format_string = '{:30} {:10.1f} {:>25} {:>10}'
        for entry in hpdl.dirents:
            self.poutput(format_string.format(entry.dname,
                                              float(entry.dsize),
                                              entry.dtype,
                                              entry.checksum))

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_path(self, _inp):
        """Execute PATH command on calculator"""
        path = self.hpc.hp_path()
        print(path)

    @cmd2.with_category(CMD_RAW_COMMAND)
    def do_cmd(self, cmd, display=True):
        """Execute specified command on calculator."""
        kcmd = 'remote host %s' % cmd
        response = self.hpc.hp_cmd(kcmd)
        if display:
            self.poutput(response)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_refresh(self, _inp):
        """Get updated directory entries"""
        current_path = self.hpdl.dir_path_list
        self.hpdl = self.hpc.hp_dir(current_path)
        self.display_directory(self.hpdl)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_dir(self, _inp):
        """Display current directory listing"""
        self.display_directory(self.hpdl)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_sdir(self, _inp):
        """Display sorted current directory listing"""
        self.display_directory(self.hpdl, sort_dir=True)

    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_cd(self, path):
        """Change directory, but only if it exists"""
        logging.debug('path = %s, self.hpdl = %s', path, self.hpdl)

        # sending invalid directory to calculator and waiting for response
        # is slow, better to indicate error locally
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype == 'Directory'] + ['', '..', '/']
        if path.strip('"') not in candidates:   # where should this be, in completer?
            self.perror('Please select an existing directory', apply_style=True)
            return

        current_path = self.hpdl.dir_path_list

        if not path:
            # just like Linux, go home
            new_path = ['HOME']

        # TODO: handle both relative and absolute path spec
        if path == '..':
            if len(current_path) > 1:
                new_path = current_path[:-1]
            else:
                new_path = ['HOME']
        elif path == '/' or path is None or path == '':
            new_path = ['HOME']
        else:
            current_path.append(path)
            new_path = current_path

        # change dir
        logging.debug('new_path = %s', new_path)
        self.hpdl = self.hpc.hp_dir(new_path)
        self.display_directory(self.hpdl)

    def complete_cd(self, text, _line, _begidx, _endidx):
        """Completer for do_cd()"""
        candidates = [x.dname for x in self.hpdl.dirents if x.dtype == 'Directory']
        if not text:
            completions = candidates
        else:
            completions = [f for f in candidates if f.startswith(text)]
        return completions

    # FIXME: This is just for test, delete?
    @cmd2.with_category(CMD_MAIN_COMMANDS)
    def do_rdir2(self, _):
        """fetch formatted RDIR command as a Python data structure."""
        cmd = 'rdir'
        resp_stdout = self.hpc.hp_cmd(cmd)
        print(resp_stdout)

        hpdl = hpdirlist.HPDirList()
        hpdl.parse_rdir(resp_stdout)

        print(hpdl)

        hppath = hputils.pc_dir_from_calc_dir(hpdl.dir_path_list)
        self.prompt = 'hp50g:{} >'.format(hppath)

        format_string = '{:30} {:10.1f} {:>20} {:>10}'

        for entry in hpdl.dirents:
            print(format_string.format(entry.dname,
                                       float(entry.dsize),
                                       entry.dtype,
                                       entry.checksum))

    def default(self, inp):
        if inp in ('x', 'q'):
            return self.do_exit(inp)


if __name__ == '__main__':
    HPShell().cmdloop()
