"""
Basic setup.py for hp50gshell package.
"""

from setuptools import setup

setup(name='hp50gshell',
      version='0.1.0',
      description='Simple Command Shell for HP50G',
      author='Frank Singleton',
      author_email='b17flyboy@gmail.com',
      packages=['hp50gshell'],
      install_requires=['pyserial', 'cmd2'],
      license="GPL3 License",
      url="https://bitbucket.org/pymaximus/hp50gshell",
      classifiers=[
          "Development Status :: 4 - Alpha",
          "Intended Audience :: Developers",
          "License :: OSI Approved :: GPL3 License",
          "Programming Language :: Python :: 3.6",
          "Operating System :: Linux",
          "Topic :: Utilities"
      ],

      scripts=['scripts/hpshell.py']


      )
